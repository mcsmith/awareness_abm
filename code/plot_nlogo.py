import matplotlib.pyplot as plt
from csv import reader as csv_reader, writer as csv_writer
from sys import argv, stderr
from math import floor
from collections import defaultdict
from os.path import basename

CUMULATIVE_COLORS = {
        2: ['r', 'b', 'g'],
        3: ['r', 'b', 'g', 'k'],
        4: ['r', 'b', 'g', 'k'],
}
POPULATION_COLORS = {
        2: ['r', 'b', 'g', 'm'],
        3: ['r', 'b', 'k', 'g', 'm'],
        4: ['r', 'b', 'k', 'g', 'm'],
}


def plot_nlogo(data, x_label, y_label, title, label, color_num):
    # ignore first 18 rows
    # 3 categories of 4 columns
    # infected, recovered, aware
    # for each: x, y, color, pen down
    # data will have already parsed this
    fig, axes = plt.subplots()
    i = 0
    lines = []
    c = POPULATION_COLORS
    if "Cumulative" in title:
        c = CUMULATIVE_COLORS
    for category in sorted(data):
        x = data[category]['x']
        y = data[category]['y']
        my_color = c[color_num][i]
        p, = axes.plot(x, y, color=my_color, label=category)
        lines.append(p)
        i += 1
    axes.set_title(title)
    axes.set_xlabel(x_label)
    axes.set_ylabel(y_label)
    axes.legend(lines, [l.get_label() for l in lines], loc='best')
    label = label.replace('/', '-')
    label = label.replace('.', '')
    label = label.replace('%', 'percent')
    label = label.replace(' ', '_')
    fig.savefig(label + '.png')
    plt.clf()
    plt.close()


def plot_cumulative(data, file_label, color_num):
    plot_nlogo(data, "Time Steps", "Cumulative Population Percentages", "Cumulative Populations", file_label, color_num)


def plot_populations(data, file_label, color_num):
    plot_nlogo(data, "Time Steps", "Population Counts", "Populations", file_label, color_num)


for infile in argv[1:]:
    # groups of 4 columns
    # x,y, color, pen down
    # skip first 19 lines
    with open(infile, 'rb') as inf:
        stop_at_row = -1
        if "Populations" in infile:
            stop_at_row = 19
        elif "Cumulative" in infile:
            stop_at_row = 18
        first_num_in_infile = 2
        try:
            fname = basename(infile)
            first_num_in_infile = int(fname[0])
        except:
            continue
        if first_num_in_infile > 2:
            stop_at_row += 1
        r = csv_reader(inf)
        i = 1
        while i < stop_at_row:
            r.next()
            i += 1
        # read next line, get all nonempty strings, strip \" char
        label_list = r.next()
        label_list = [x.strip('"') for x in label_list if x]
        n = len(label_list)
        print infile
        print label_list
        print r.next()
        curr_data = defaultdict(lambda: {'x': [], 'y': []})
        for row in r:
            print row
            num_cols = len(row)
            for col_idx, col_val in enumerate(row):
                print "col idx:", col_idx
                curr_label_num = int(floor(col_idx / 4))
                print "curr label num ", curr_label_num
                curr_label = label_list[curr_label_num]
                if col_idx % 4 == 0:  # 'x'
                    print "curr label num ", curr_label_num
                    print "curr label ", curr_label
                    print 'x', col_val
                    if not col_val == '':
                        curr_data[curr_label]['x'].append(col_val)
                elif col_idx % 4 == 1:  # 'y'
                    print "curr label num ", curr_label_num
                    print "curr label ", curr_label
                    print 'y', col_val
                    if not col_val == '':
                        curr_data[curr_label]['y'].append(col_val)
                else:
                    pass
        if "Populations" in infile:
            # infected, susceptible, aware, recovered
            plot_populations(curr_data, infile, first_num_in_infile)
        elif "Cumulative" in infile:
            plot_cumulative(curr_data, infile, first_num_in_infile)
        else:
            print >> stderr, "unknown csv file! %s" % infile
            pass

