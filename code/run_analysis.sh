#!/usr/bin/env bash

rm *.png
python analyze_results.py $1
python analyze_two.py "summary_$1"
python plot_stats.py "avg_summary_$1"
