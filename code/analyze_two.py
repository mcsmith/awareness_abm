from csv import reader as csv_reader, writer as csv_writer
from collections import defaultdict
from numpy import average
from re import sub
from sys import argv


infile = argv[1]
outfile = "avg_" + infile

# note the following order in infile
# static_columns_desired = ["ignore", "hide", "flee", "network-member-percent", "percentage-infected-media-broadcast"]
# then...
avg_columns_desired = ["[step]", "tick-of-media-broadcast", "max-aw-count", "max-inf-count", "max-inf-tick", "max-aw-tick", "farthest-infected-distance", "time-taken-to-reach-bottom-right", "aw-r0", "r0", "disease-spread"]
#ignore,hide,flee,network-member-percent,percentage-infected-media-broadcast,[step],tick-of-media-broadcast,max-aw-count,max-inf-count,max-inf-tick,max-aw-tick,farthest-infected-distance,time-taken-to-reach-bottom-right,aw-r0,r0,disease-spread

def get_behavior_key(row):
    return tuple(map(int, row[:3]))


def get_network_key(row):
    return int(row[3])


def get_media_key(row):
    return int(row[4])


# key_type -> dict of {key -> dict of {col_name -> list of data}}
data = defaultdict(lambda: defaultdict(lambda: defaultdict(list)))
with open(infile, 'rb') as inf:
    r = csv_reader(inf)
    headers = r.next()
    print headers
    for row in r:
        b = get_behavior_key(row)
        n = get_network_key(row)
        m = get_media_key(row)
        for key_type, key in zip(['Behavior', '% on social network', '% when media blasts'], [b,n,m]):
            for col_name in avg_columns_desired:
                idx = headers.index(col_name)
                datum = float(row[idx])
                #print "%s %s %s -> %s" % (key_type, key, col_name, datum)
                data[key_type][key][col_name].append(datum)

with open(outfile, 'wb') as outf:
    w = csv_writer(outf)
    new_headers = ['key_type', 'key', '% runs with media blast'] + avg_columns_desired
    w.writerow(new_headers)
    for key_type, d in sorted(data.iteritems(), key=lambda x: x[0]):
        for key, second_d in sorted(d.iteritems(), key=lambda x: x[0]):
            out_row = [key_type, key]
            total = 0
            count_with_blast = 0
            for col_name in avg_columns_desired:
                my_list = second_d[col_name]
                if 'media' in col_name:
                    total = len(my_list)
                    count_with_blast = total - my_list.count(-1)
                    avg = average([i for i in my_list if not i == -1])
                else:
                    avg = average(my_list)
                #print "%s %s %s -> %s" % (key_type, key, col_name, avg)
                out_row.append(avg)
            #nh = ['key_type', 'key'] + avg_columns_desired
            #print zip(nh, out_row)
            out_row.insert(2, float(count_with_blast)/total)
            #print zip(new_headers, out_row)
            w.writerow(out_row)

