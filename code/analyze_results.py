from csv import reader as csv_reader, writer as csv_writer
import sys
from re import sub
from collections import defaultdict
from scipy.stats import ttest_ind
import numpy as np


infile = sys.argv[1]
summary_file = "summary_" + infile
print summary_file
data = defaultdict(list)

def make_key(row, headers):
    return tuple([
        int(row[headers.index("aw-react-ignore-percent")]),
        int(row[headers.index("aw-react-hide-percent")]),
        int(row[headers.index("aw-react-flee-percent")]),
        int(row[headers.index("network-member-percent")]),
        int(row[headers.index("percentage-infected-media-broadcast")])
        ])

fi = open(infile, 'rb')
mydata = fi.read()
fi.close()
wahoo = 'fixed_' + infile
fo = open(wahoo, 'wb')
fo.write(mydata.replace('\x00', ''))
fo.close()

static_columns_desired = ["ignore", "hide", "flee", "network-member-percent", "percentage-infected-media-broadcast"]
avg_columns_desired = ["[step]", "tick-of-media-broadcast", "max-aw-count", "max-inf-count", "max-inf-tick", "max-aw-tick", "farthest-infected-distance", "time-taken-to-reach-bottom-right", "aw-r0", "r0"]

with open(wahoo, 'rb') as inf:
#with open(infile, 'rb') as inf:
    r = csv_reader(inf)
    [r.next() for i in range(6)]
    headers = r.next()
    print headers
    with open(summary_file, 'wb') as smf:
        sm_csv = csv_writer(smf)
        sm_csv.writerow(static_columns_desired + avg_columns_desired + ['disease-spread'])
        i = 0
        skipped = 0
        for row in r:
            step = int(row[headers.index("[step]")])
            if step <= 0:
                skipped += 1
                continue  # disregard completely, ran invalid params
            k = make_key(row,headers)
            data[k].append(row)
            i += 1
        print "added %d rows, skipped %d -> keys:%d" % (i, skipped, len(data))
        for key, rows in sorted(data.iteritems(), key=lambda x: map(int, x[0])):
            rows = list(rows)
            data_for_avgs = defaultdict(list)
            for row in rows:
                for a in avg_columns_desired:
                    data_for_avgs[a].append(float(row[headers.index(a)]))
                # custom disease speed
                data_for_avgs['disease-spread'].append(1-float(row[headers.index("time-taken-to-reach-bottom-right")])/float(row[headers.index("[step]")]))
            avgs = {}
            for my_key, data in data_for_avgs.iteritems():
                avgs[my_key] = np.average(data)
            sm_csv.writerow(list(key) + map(lambda x: avgs[x], avg_columns_desired + ['disease-spread']))

