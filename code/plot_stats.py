from csv import reader as csv_reader, writer as csv_writer
from sys import argv
import matplotlib.pyplot as plt
from collections import defaultdict


translations = {
    '% on social network': "% of pop. on social media",
    '% when media blasts': "Infection % triggering news media blast",
    "% runs with media blast": "% of runs in which news media broadcast",
    "[step]": "Epidemic length",
    "tick-of-media-broadcast": "News media broadcast time",
    "max-aw-count": "Max. awareness incidence",
    "max-inf-count": "Max. disease incidence",
    "max-inf-tick": "Time of disease max. incidence",
    "max-aw-tick": "Time of awareness max. incidence",
    "farthest-infected-distance": "Farthest distance disease traveled",
    "time-taken-to-reach-bottom-right": "Time for disease to cross lattice",
    "aw-r0": "Repr. rate of awareness",
    "r0": "Repr. rate of disease",
    "disease-spread": "% of epidemic spent at max. geographic coverage"
}

def make_patch_spines_invisible(ax):
        ax.set_frame_on(True)
        ax.patch.set_visible(False)
        for sp in ax.spines.values():
            sp.set_visible(False)

infile = argv[1]
# key type -> col_name -> list of(key, val)
data = defaultdict(lambda: defaultdict(list))
with open(infile, 'rb') as inf:
    c = csv_reader(inf)
    headers = c.next()
    print headers
    for row in c:
        key_type = row[0]
        key = row[1]
        for i, col_name in enumerate(headers[2:]):
            val = row[i+2]
            data[key_type][col_name].append((key, val))
for key_type, d in data.iteritems():
    if key_type == 'Behavior':
        continue
    fig, axes = plt.subplots()
    fig.subplots_adjust(right=0.75)
    last_axes = None
    lines = []
    i = 0
    for col_name, my_list in sorted(d.iteritems(), key=lambda x: x[0], reverse=True):
        if col_name == 'r0' or col_name == 'max-inf-count':
            print 'staying'
        elif 'network' in key_type and col_name == '[step]':
            print 'staying'
        else:
            print 'skipping %s' % col_name
            continue
    	keys, vals = zip(*my_list)
        if col_name in translations:
            c = translations[col_name]
        else:
            c = col_name
        if key_type in translations:
            k = translations[key_type]
        else:
            k = key_type
        if last_axes is not None:
            label += " and %s" % (col_name)
            title += " and %s" % (c)
        else:
            label = "%s vs %s" % (key_type, col_name)
            title = "%s vs %s" % (k, c)
        if i == 0:
            mycolor = 'r'
            new_axes = axes
        elif i == 1:
            new_axes = axes.twinx()
            mycolor = 'b'
        elif i == 2:
            mycolor = 'g'
            new_axes = axes.twinx()
            # Offset the right spine of par2.  The ticks and label have already been
            # placed on the right by twinx above.
            new_axes.spines["right"].set_position(("axes", 1.2))
            # Having been created by twinx, par2 has its frame off, so the line of its
            # detached spine is invisible.  First, activate the frame but make the patch
            # and spines invisible.
            make_patch_spines_invisible(new_axes)
            # Second, show the right spine.
            new_axes.spines["right"].set_visible(True)
        p, = new_axes.plot(keys, vals, color=mycolor, label=c)
        lines.append(p)
        for tl in new_axes.get_yticklabels():
            tl.set_color(mycolor)
        if i == 2:
            if len(title) > 50:
                title = title.replace(" and", ",\n", 1)
                title = title.replace(" and", ", and", 1)
            new_axes.set_title(title)
        new_axes.set_xlabel(k)
        new_axes.set_ylabel(c)
        new_axes.yaxis.label.set_color(mycolor)
        last_axes = new_axes
        i += 1
    last_axes.legend(lines, [l.get_label() for l in lines], loc='best')
    label = label.replace('/', '-')
    label = label.replace('.', '')
    label = label.replace('%', 'percent')
    label = label.replace(' ', '_')
    fig.savefig(label + '.png')
    plt.clf()
    plt.close()

