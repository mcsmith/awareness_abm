% This is LLNCS.DEM the demonstration file of
% the LaTeX macro package from Springer-Verlag
% for Lecture Notes in Computer Science,
% version 2.4 for LaTeX2e as of 16. April 2010
%
\documentclass{llncs}
%
\usepackage{makeidx}  % allows for indexgeneration
\usepackage{hyperref}
\bibliographystyle{splncs03}
\usepackage{graphicx}
\graphicspath{ {figures/}{./} }
%
\begin{document}
%
\frontmatter          % for the preliminaries
%
\pagestyle{headings}  % switches on printing of running heads

\mainmatter              % start of the contributions
%
\title{Modeling Influenza by Modulating Flu Awareness}
%
\titlerunning{Modeling Influenza by Modulating Flu Awareness}  % abbreviated title (for running head)
%                                     also used for the TOC unless
%                                     \toctitle is used
%
\author{Michael C. Smith\inst{1} \and David A. Broniatowski\inst{1}}
%
\authorrunning{Michael C. Smith et al.} % abbreviated author list (for running head)
%
%%%% list of authors for the TOC (use if author list has to be modified)
\tocauthor{Michael C. Smith, David A. Broniatowski}
%
\institute{The George Washington University, Washington, DC, USA\\
\email{\{mikesmith,broniatowski\}@gwu.edu}}

\maketitle              % typeset the title of the contribution

\begin{abstract}
It is important for public health officials to follow both the incidence of disease and the public's perception of it, especially in the Internet-connected age. In the specific context of influenza, disease surveillance through social media has proven effective, but public awareness of influenza and its effects are not well understood. We build upon the existing Epstein model of coupled contagion with the aim of including modern media mechanisms for awareness transmission. Our agent-based model captures the unique effects of news media and social media on disease dynamics, and suggests potential areas for policy intervention to modulate the spread of the flu.
\keywords{agent-based modeling, influenza, awareness, coupled contagion, surveillance}
\end{abstract}
%

\section{Introduction}
%
Accurate and timely surveillance methods are important for monitoring potential epidemics.  Influenza poses an annual threat across the US, leading much recent work to focus on observing the flu in particular  \cite{Brownstein:web-public-health,Dugas:gft}.   Several authors draw on the Internet and social media data to perform this surveillance on flu and other diseases  \cite{Broniatowski:flu2012,Brownstein:web-public-health,Corley:flumentions,Dredze:2014fk,Lee:2013:RDS:flucancer} because these media are readily available and offer a means of quickly and easily transmitting information.
\par
Public health officials' ultimate goal is not merely to track a disease during an epidemic, but also to be able to attenuate the spread of the disease.  This latter goal depends on effective communication: informing the public of appropriate behaviors to take that may so attenuate the spread; or in other words, it depends on measuredly making the public aware.  Thus, we aim to better understand how the spread of this influenza awareness interacts with the spread of the disease when considering the effects of Internet-enabled media.  In the Epstein model of coupled contagion (CC), the spread of a disease and awareness of the disease interact in emergent ways \cite{Epstein:cc}.  We motivate adding such Internet-enabled media into the CC model as potential mechanisms to modulate the spread of the disease.  The outline of the rest of this paper is as follows: first we detail work in the area in a literature review, introducing among other things the CC model and the data from which we drew inspiration; next we provide the details of our model augmentation; third, we use our model to examine how these media might influence the spread of the flu; and finally, we conclude and identify directions for future work.

\section{Literature Review}

Information transmitted via the Internet and social media is not limited strictly to information about disease incidence.  Hatfield described how emotions are "contagions" that spread through social interactions \cite{Hatfield1994emotional}; it follows that awareness of a disease is an instance of such a contagion.  Because so many social interactions occur online, awareness of disease may also travel via the internet and social media.  Along with other prominent work describing the role of concerned awareness on the spread of disease, Epstein's CC model \cite{Epstein:cc} motivated us to recently extend our social media flu surveillance system \cite{Dredze:2014fk} to measure signals of influenza awareness \cite{Smith:2016fv}.  Using the social media microblogging site Twitter, Smith et al. specifically provided preliminary results comparing influenza awareness data, Twitter influenza tracking data (infection), and online news media data during the 2012-2013 flu season \cite{Smith:ddd,Smith:2016fv}.  We showed that influenza awareness trends can be separated from influenza infection trends, and that news media correlate strongly with signals of influenza awareness \cite{Smith:2016fv}.  While Broniatowski et al. have previously shown that influenza surveillance using Twitter is an effective predictor of actual influenza infection rates \cite{Broniatowski:flu2012}, little work has been done to examine influenza awareness and relate it to behavioral adaptations.\par
%
Prior work has shown that integrating behavioral choice into models of other diseases yields results that reflect empirical findings (as in the case of AIDS \cite{Kremer1996integrating}).  Furthermore, Qinling Yan and colleagues have found that media coverage, in particular, may have driven people's behavior during the H1N1 outbreak \cite{Yan20161}.  Along with the findings of our previous work, this current conversation motivates the novel contribution of this paper: augmenting the CC model with news media and social media as potential mechanisms to modulate the spread of the flu.

\subsection{Epstein's Coupled Contagion Model}

Classical epidemiological models use differential equations to track populations of Susceptible, Infected, and Recovered individuals \cite{Kermack700}.
%, as shown in Fig. 1.
%\begin{figure}
%\begin{center}
%\includegraphics[width=0.75\textwidth]
%{sirwiki.png}
%\caption{Plots of time versus SIR populations. Blue = susceptible, green = infected, red = recovered.  Image source: https://commons.wikimedia.org/wiki/File:Sirsys-p9.png No machine-readable author provided. Bye~commonswiki assumed (based on copyright claims). [Public domain, permission implicit], via Wikimedia Commons.}
%\end{center}
%\end{figure}
Epstein et al. introduced the idea that "concerned awareness" of a disease spreads like a contagion, interacting with the dynamics of the disease itself \cite{Epstein:cc}. Epstein et al. showed that incorporating behavioral adaptations that are driven by this awareness into models of epidemic prevalence have a qualitatively marked effect on how disease may spread. Considered adaptations include ignoring the disease, self-isolating or hiding from the disease, and fleeing from it. \par
% 
Specifically, Epstein and colleagues showed that this coupled contagion model can be used to generate nontrivial trends such as multiple waves of infection. For example, Fig. \ref{fig:cc_multiple_waves} shows how large numbers of hiders can significantly reduce the infected population \cite{Epstein:cc}.

\begin{figure}
\begin{center}
\includegraphics[width=0.75\textwidth]
{cc_multiple_waves.png}
\caption{"In the idealized run...susceptible individuals (blue-curve) self-isolate (black curve) through fear as the infection of disease proper grows (red curve)" Image source: \cite{Epstein:cc} [Public domain, permission implicit]
\label{fig:cc_multiple_waves}}
\end{center}
\end{figure}

CC assumed awareness required physical contact for transmission \cite{Epstein:cc}, but the interconnectedness of modern society provides additional social opportunities that go beyond spatial proximity.  In the next section we extend the agent-based model (ABM) approach of Epstein et al. \cite{Epstein:cc} by modeling media coverage and social media information exchange.  Epstein showed that behavioral reactions to awareness affects disease spread \cite{Epstein:cc}; we show that including the additional channels for awareness transmission changes the effects of awareness on behavior.

\section{Methods}

We iteratively build from the standard Susceptible-Infected-Recovered (SIR) model in the NetLogo model library \cite{Wilensky:1999,Wilensky:2011} to an augmented version of CC that reflects today's interconnected society.  The successive constructions are in the following subsections, with each being evaluated against the previous version of the model, holding all parameters constant between model changes where possible.  We also conduct manual and automated sensitivity analyses.  Steps 1-3 recreate the CC model.  Steps 4-5 are our augmentations. \footnote{These descriptions are intended to provide insight into the motivation underlying these models. See \url{https://bitbucket.org/mcsmith/awareness\_abm} for the full code that details all particularities of implementation.}

\subsection{Epstein's CC Model}
\subsubsection{Standard SIR}
SIR models are standard in epidemiology, dividing the population into Susceptible, Infected, and Recovered populations \cite{Kermack700}. One agent-based approach to SIR modeling (the "epiDEM basic" model in the NetLogo library) has agents, representing people, that move in a random direction a random number of steps between 0 and their maximum movement parameter $A$, and after moving they then interact with a random other agent in the area if any others are there \cite{Wilensky:2011}. If an agent is infected and another susceptible, the infected agent has a probability $B$ to infect the susceptible agent. Infected agents recover after being infected for $C$ time steps. Agents are initialized in the infected state (as opposed to in the susceptible state) with density $D$, meaning $D$\% of agents start infected. The model run ends when disease prevalence equals zero.
\subsubsection{Contagion of Coupled Awareness}
In addition to disease contagion, Epstein \cite{Epstein:cc} modeled concerned awareness of the disease like a contagion, with corresponding parameters $E$, $F$, and $G$ respectively indicating the probabilities of transmission, recovery rate, and initial density respectively. Agents can be aware of a disease, infected with the disease, or both, and agents can "catch" awareness from another agent who is aware and/or infected.  Contrasting with the epiDEM model \cite{Wilensky:2011}, CC introduced a set world size to the model to allow measuring if the disease spreads fully across the world, and also introduced the notion of a disease beginning with a single agent \cite{Epstein:cc} instead of relying on $D$.
\subsubsection{Behavioral Response to Awareness (e.g. hiding, fleeing, ignoring)}
Epstein modeled behavioral responses to awareness and captured how these responses modified the spread of the disease \cite{Epstein:cc}. The population of agents is divided into $H / 100$ \% hiders, $I / 100$ \% fleers, and $J / 100$ \% ignorers (such that $H+I+J = 100$). If ignorers become aware, they move randomly as they normally would. Hiders self-isolate for the duration of their awareness. By contrast, fleers move as quickly as possible to a new randomly-selected location a distance of $K$ units away if they become aware.

\subsection{Augmenting the CC Model with Media}
\subsubsection{News Media Broadcasts May Spread Awareness}
This is the first novel modification of the CC model, corresponding to our observation that news media coverage correlates with flu awareness. Specifically, raw counts of flu stories from the news aggregator \url{http://www.newslibrary.com/} have a distinct peak that coincide with measured influenza awareness, with little activity at other times \cite{Smith:2016fv}.  We therefore simulate news media coverage by broadcasting awareness to a percentage of agents, $L$.  Agents that listen to the news media become aware of the flu.  We broadcast once the disease incidence has reached a percentage $S$, similar to how news media might react.
\subsubsection{Social Media Connections May Spread Awareness}
A second novel addition to the CC model model is a communication network through which agents share simulated social media (e.g., Tweets). Since it is not a requirement that two agents that communicate via social media be geographic neighbors, the interaction network superimposed on the world allows for connections between agents that are otherwise not physically adjacent.  We introduce another dimension of agents: whether or not they use social media, with initial density $M$.  Membership in the network is constant throughout the model runs, with $M$\% of agents included as vertices.  In our model, the degree distribution of social media users follows a power law, consistent with the findings of Saroop et al. \cite{Saroop:twitter}.  After agents move and possibly spread awareness and infection to their local neighbors with probabilities $E$ and $B$ respectively, agents that use social media transmit awareness with probability $N$ to their respective "neighbors" along this social media layer.

\subsection{Automated Sensitivity Analysis}
We use NetLogo's built-in BehaviorSpace tool \cite{Wilensky:1999} for automatic sensitivity analysis of the augmented model.  We vary (from 0 to 100 by steps of 10) the behavior parameters $H$, $I$, $J$, the network membership parameter $M$, and the news media broadcast parameter $S$, keeping all other parameters identical to the runs reported in Epstein's CC paper \cite{Epstein:cc}.  Note that for simplicity we assume a constant $L$ in these automatic runs, specifically 100\%.  We run each parameterization ten times, and we record the random seed used in each run along with the following metrics: a) when the media broadcast occurred; b,c) the maximum disease incidence and time; d,e) the maximum awareness incidence and time; f) the reproductive rate of the disease ($R_0$) \cite{Epstein:cc} \cite{Diekmann:repr-rate}; g) the reproductive rate of awareness \cite{Epstein:cc}; h) the time taken for the disease to cross the model's world fully.

\section{Results}

\subsection{Replicating the CC Model}
Our reimplementation of the CC ABM, using every parameter specified in the paper by Epstein et al. \cite{Epstein:cc}, replicates their findings. Without behavior, awareness leads infection \cite{Epstein:cc}. When behavior is included, such as when 90\% of agents ignore and 10\% of agents hide, the rate of disease spread and total incidence both decrease (Fig. \ref{fig:step3}). The curves in these cases are qualitatively similar to each other; they do not display differences in timing and width.  Other examples of how behavior influences trends are as follows: more fleers increases the rate of spread of disease, and more hiders decreases disease spread but also limits the spread of awareness \cite{Epstein:cc}.

\begin{figure}
\begin{center}
\includegraphics[width=0.8\textwidth]
{step3-screenshot_crop.png}
\caption{Screenshot of example results from step 3: awareness with behavior, with 90\% ignorers (no behavior) and 10\% hiders.  Awareness and infection have distinctly different-sized peaks in upper-right because the hiders contribute to the reduced cumulative incidence (see left)
\label{fig:step3}}
\end{center}
\end{figure}

\subsection{Augmenting with News Media}
Because we successfully recreate the CC model, we now consider our augmentations.  When news media broadcasting is added, overall, it makes peak awareness earlier and more of a difference between awareness and infection.  Fig. 
\ref{fig:step4} shows a typical result using the 90\% ignore, 10\% hide parameterization previously cited.  With 10\% of agents listening to news media ($L$), peak awareness is earlier and higher compared to the current state of infection, but the infection curve seems to be unaffected.

\begin{figure}
\begin{center}
\includegraphics[width=\textwidth]
{step4-screenshot_crop.png}
\caption{Screenshot of example results from step 4: example of media broadcast (vertical line on plots) affecting cumulative trends (left) and  population trends (right)
\label{fig:step4}}
\end{center}
\end{figure}

In a manual sensitivity analysis these results are robust to variation in the $L$ parameter, but depend on $S$.  Thus, news media broadcasts appear to influence awareness.  The automated sensitivity analysis confirms this and the connection to disease.  It reveals that (averaged across all other parameters) the later the broadcast, the faster the disease reproduces ($R_0$) and the higher maximum disease incidence; see Fig. \ref{fig:sensitivity_4}.

\begin{figure}
\begin{center}
\includegraphics[width=0.8\textwidth]{percent_when_media_blasts_vs_max-inf-count_and_r0.png}
\caption{Plot of the infection percentage triggering the news media broadcast vs the maximum disease incidence (red) and the reproductive rate of the disease (blue)
\label{fig:sensitivity_4}}
\end{center}
\end{figure}

\subsection{Augmenting with Social Media}

We next include social media networking. In a typical run, even when only 10\% of participants use social media, we see that the network acts to spread awareness across the physical landscape and the effect of media broadcasting is overwhelmed (Fig. \ref{fig:step_5}).  We perform manual sensitivity analysis, and find that this augmentation influences awareness more as the size of the network increases.  Specifically, the width of the awareness peak widens and increasingly precedes that of the disease.  

\begin{figure}
\begin{center}
\includegraphics[width=\textwidth]
{step5-screenshot_crop.png}
\caption{Screenshot of example results from step 5. Note influence of network on awareness trends, before media broadcast.
\label{fig:step_5}}
\end{center}
\end{figure}

In our automated sensitivity analysis (once again averaged over all other parameters), we see that as membership in the social network increases, both the reproductive rate of disease and the maximum disease incidence decrease.  However, social media membership has a positive, somewhat erratic, relationship with the epidemic length.  These relationships are displayed in Fig. \ref{fig:sensitivity_5}.

\begin{figure}
\begin{center}
\includegraphics[width=0.8\textwidth]
{percent_on_social_network_vs_r0_and_max-inf-count_and_[step].png}
\caption{Plot of the percent of population on social media vs reproductive rate of disease (red), maximum disease incidence (blue), and epidemic length (green)
\label{fig:sensitivity_5}}
\end{center}
\end{figure}

\section{Discussion}
Adding Internet-enabled news media coverage and social media connections to the CC model is a conceptual advance that may help explain awareness of disease in modern society.  We augment the CC model to show how behavioral reactions to awareness influence the spread of disease, and show how news media and social media influence the spread of awareness.  Both news media and social media each enable awareness to potentially spread much more quickly than infection.  A key insight is the decoupling of geographic location with awareness transmission.  If used to spread awareness, news media and social media provide powerful tools for officials to influence a population, and thus (depending on how the population reacts) also to potentially influence the spread of disease itself.  However, as social media membership increases so does epidemic length.  This highlights the uncontrollability of social media, and the need to present and guide reactions to disease; or to speak our model's language, it highlights the need to guide agents to hide or flee in appropriate situations.

\subsection{Future Work}
We take inspiration from the available influenza awareness, flu infection, and news media trends, but future work is still needed to more exactly model these trends.  Such work may involve discovery of any additional confounding issues or variables involved that the hypothesized model may omit, which could lead to further understanding of what drives awareness. Possible suggestions to be evaluated are differing rates of awareness transmission before / after media broadcast, having agents' centrality in the social network influence their "success" transmitting awareness, and having social media transmission depend on local incidence of the contagion(s).\par
%
Incorporating spatial statistics and information like population density, disease incidence density, and distance in a map of the USA may also be worth exploring if goals align with fitting observed data. It would be also worth examining the structure of Twitter to see if most users have geographically close connections, as our work disregarded this spatial component. \par
%
Another major area for improvement would be investigating the impact of diverting awareness into separate signals meant to target possible behaviors.  To again speak our model's language, public health officials may want to incite hiding or fleeing depending on the situation.  Because social media was observed to be so overwhelming, this tactical messaging would correspond directly to how media might heighten or dampen that overwhelming effect.  A model could incorporate the idea that an agent listens to the type of awareness he first receives, be it tactical media or uncontrolled social media; this may offer further opportunities for influencing disease spread.

\section{Conclusion}
Our ABM updates the CC model to include modern awareness transmission mechanisms.  Specifically, we show how news media and social network information can each influence the spread of awareness, leading to drastic increases in incidence and rate of spread therein as compared to the original CC model.  This update suggests that health providers and public health officials may be able to leverage these tools to spread awareness of a disease, and allows policy makers to better understand the consequences of how media technology interacts with the spread of disease.

\section{Acknowledgements}
The authors would like to acknowledge Dr. Mark Dredze for allowing us access to the HealthTweets awareness trends, and Dr. Joshua Epstein for helpful feedback.

\bibliography{mcs_sbp_2016}

\clearpage
\addtocmark[2]{Author Index} % additional numbered TOC entry
\end{document}
