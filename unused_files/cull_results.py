from csv import reader as csv_reader, writer as csv_writer
import sys
from re import sub
from collections import defaultdict
from scipy.stats import ttest_ind


infile = sys.argv[1]
outfile = sub(r"\.csv$", "_culled.csv", infile)
ignorefile = sub(r"\.csv$", "_ignore.csv", infile)
inbetweenfile = sub(r"\.csv$", "_inbetween.csv", infile)
print outfile
data = defaultdict(list)
high_ignore = defaultdict(list)
other_ignore = defaultdict(list)
high_hide = defaultdict(list)
other_hide = defaultdict(list)
low_hide = defaultdict(list)
othertwo_hide = defaultdict(list)
high_flee = defaultdict(list)
other_flee = defaultdict(list)
low_flee = defaultdict(list)
othertwo_flee = defaultdict(list)

def make_key(row, headers):
    return tuple([
        int(row[headers.index("aw-react-ignore-percent")]),
        int(row[headers.index("aw-react-hide-percent")]),
        int(row[headers.index("aw-react-flee-percent")]),
        #row[headers.index("network-member-percent")],
        #row[headers.index("percentage-infected-media-broadcast")]
        ])

def process(data, name, headers):
            print name
            blah = 0
            wanted_res_count = 0
            blahblah = []
            for key, rows in sorted(data.iteritems(), key=lambda x: map(int, x[0])):
                rows = list(rows)
                valid_check = []
                diffs = []
                for row in rows:
                    blah += 1
                    max_inf_count = int(row[headers.index("max-inf-count")])
                    max_aw_count = int(row[headers.index("max-aw-count")])
                    max_inf_tick = int(row[headers.index("max-inf-tick")])
                    max_aw_tick = int(row[headers.index("max-aw-tick")])
                    diff = max_inf_tick - max_aw_tick
                    diffs.append(diff)
                    if max_aw_tick < 5 or max_inf_tick >= max_aw_tick:
                        #q.writerow(new_row)
                        valid_check.append(False)
                        blahblah.append(0)
                        continue
                    #print >> sys.stderr, "%s has diff:%d" % (row[0], diff)
                    #w.writerow(new_row)
                    wanted_res_count += 1
                    blahblah.append(1)
                    valid_check.append(True)
                #print "%s -> %d of %d = %f ... %s" % (str(list(zip(['ignore:', 'hide:', 'flee:'], key))), sum(valid_check), len(valid_check), float(sum(valid_check)) / len(valid_check), str(diffs))
            print "total:%d of %d = %f" % (wanted_res_count, blah, float(wanted_res_count)/blah)
            return blahblah

with open(infile, 'rb') as inf:
    r = csv_reader(inf)
    [r.next() for i in range(6)]
    headers = r.next()
    inbetweenf = open(inbetweenfile, 'wb')
    with open(outfile, 'wb') as outf:
        with open(ignorefile, 'wb') as ignoref:
            w = csv_writer(outf)
            q = csv_writer(ignoref)
            z = csv_writer(inbetweenf)
            w.writerow(headers + ['diff'])
            q.writerow(headers + ['diff'])
            z.writerow(headers + ['diff'])
            total_res_count = 0
            valid_res_count = 0
            wanted_res_count = 0
            for row in r:
                total_res_count += 1
                max_inf_count = int(row[headers.index("max-inf-count")])
                max_aw_count = int(row[headers.index("max-aw-count")])
                max_inf_tick = int(row[headers.index("max-inf-tick")])
                max_aw_tick = int(row[headers.index("max-aw-tick")])
                diff = max_inf_tick - max_aw_tick
                new_row = row + [diff]
                if max_inf_count == -1 or max_inf_count == 0:
                    continue  # disregard completely, ran invalid params
                if max_inf_count < 10 or max_aw_count < 10:
                    # disregard because infection didn't reach sizable levels
                    continue
                valid_res_count += 1
                k = make_key(row,headers)
                data[k].append(row)
                if k[0] >= 95:
                    high_ignore[k].append(row)
                else:
                    other_ignore[k].append(row)
                if k[1] >= 40:
                    high_hide[k].append(row)
                else:
                    other_hide[k].append(row)
                if k[1] <= 10:
                    low_hide[k].append(row)
                else:
                    othertwo_hide[k].append(row)
                if k[2] >= 40:
                    high_flee[k].append(row)
                else:
                    other_flee[k].append(row)
                if k[2] <= 10:
                    low_flee[k].append(row)
                else:
                    othertwo_flee[k].append(row)

                if max_aw_tick < 5 or max_inf_tick >= max_aw_tick:
                    #q.writerow(new_row)
                    continue
                print >> sys.stderr, "%s has diff:%d" % (row[0], diff)
                #w.writerow(new_row)
                wanted_res_count += 1
            print >> sys.stderr, "of %s total rows, %s valid, %s remain" % (total_res_count, valid_res_count, wanted_res_count)
            print >> sys.stderr, "checking for randomness by key..."
            wanted_res_count = 0
            a = process(high_ignore, "high ignore", headers)
            b = process(other_ignore, "not high ignore", headers)
            print ttest_ind(a,b)
            a = process(high_hide, "high hide", headers)
            b = process(other_hide, "not high hide", headers)
            print ttest_ind(a,b)
            a = process(low_hide, "low hide", headers)
            b = process(othertwo_hide, "not low hide", headers)
            print  ttest_ind(a,b)
            a = process(high_flee, "high flee", headers)
            b = process(other_flee, "not high flee", headers)
            print ttest_ind(a,b)
            a = process(low_flee, "low flee", headers)
            b = process(othertwo_flee, "not low flee", headers)
            print ttest_ind(a,b)
            # for each key, determine % that are valid
            blah = 0
            for key, rows in sorted(data.iteritems(), key=lambda x: map(int, x[0])):
                rows = list(rows)
                valid_check = []
                diffs = []
                for row in rows:
                    max_inf_count = int(row[headers.index("max-inf-count")])
                    max_aw_count = int(row[headers.index("max-aw-count")])
                    max_inf_tick = int(row[headers.index("max-inf-tick")])
                    max_aw_tick = int(row[headers.index("max-aw-tick")])
                    diff = max_inf_tick - max_aw_tick
                    diffs.append(diff)
                    if max_aw_tick < 5 or max_inf_tick >= max_aw_tick:
                        #q.writerow(new_row)
                        valid_check.append(False)
                        continue
                    print >> sys.stderr, "%s has diff:%d" % (row[0], diff)
                    #w.writerow(new_row)
                    wanted_res_count += 1
                    valid_check.append(True)
                print "%s -> %d of %d = %f ... %s" % (str(list(zip(['ignore:', 'hide:', 'flee:'], key))), sum(valid_check), len(valid_check), float(sum(valid_check)) / len(valid_check), str(diffs))
                if all(valid_check):
                    for row in rows:
                        max_inf_count = int(row[headers.index("max-inf-count")])
                        max_aw_count = int(row[headers.index("max-aw-count")])
                        max_inf_tick = int(row[headers.index("max-inf-tick")])
                        max_aw_tick = int(row[headers.index("max-aw-tick")])
                        diff = max_inf_tick - max_aw_tick
                        new_row = row + [diff]
                        w.writerow(new_row)
                    wanted_res_count += len(rows)
                elif not any(valid_check):
                    for row in rows:
                        max_inf_count = int(row[headers.index("max-inf-count")])
                        max_aw_count = int(row[headers.index("max-aw-count")])
                        max_inf_tick = int(row[headers.index("max-inf-tick")])
                        max_aw_tick = int(row[headers.index("max-aw-tick")])
                        diff = max_inf_tick - max_aw_tick
                        new_row = row + [diff]
                        q.writerow(new_row)
                else:
                    # in between
                    for row in rows:
                        max_inf_count = int(row[headers.index("max-inf-count")])
                        max_aw_count = int(row[headers.index("max-aw-count")])
                        max_inf_tick = int(row[headers.index("max-inf-tick")])
                        max_aw_tick = int(row[headers.index("max-aw-tick")])
                        diff = max_inf_tick - max_aw_tick
                        new_row = row + [diff]
                        z.writerow(new_row)
    inbetweenf.close()


